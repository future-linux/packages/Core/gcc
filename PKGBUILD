# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=(gcc gcc-libs gcc-fortran gcc-go gcc-gnat gcc-gdc gcc-m2 gcc-objc gcc-lto-dump gcc-libgccjit gcc-rust)
pkgbase=gcc
pkgver=14.2.0
pkgrel=1
pkgdesc="The GNU Compiler Collection"
arch=('x86_64')
url="https://gcc.gnu.org"
license=('GPL-3.0-with-GCC-exception' 'GFDL-1.3-or-later')
makedepends=('binutils' 'mpc' 'isl' 'python' 'zstd' 'gcc-gnat' 'gcc-gdc' 'doxygen' 'rustc')
options=('!emptydirs' '!lto')
source=(https://ftp.gnu.org/gnu/${pkgname}/${pkgbase}-${pkgver}/${pkgbase}-${pkgver}.tar.xz
    gcc-ada-repro.patch
    c89
    c99)
sha256sums=(a7b39bc69cbf9e25826c5a60ab26477001f7c08d85cec04bc0e29cabed6f3cc9
    1773f5137f08ac1f48f0f7297e324d5d868d55201c03068670ee4602babdef2f
    aa3327fd8626acf2a28fa75c0b3ed99fcffb1bbcbe6974505f050ee751ba4154
    2861971c0ef2c011ce10c98e8cf7b5fd1d3d3c4eddcfe0a01ed9d76940f67638)

prepare() {
    cd ${pkgbase}-${pkgver}

    sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in

    # Reproducible gcc-ada
    patch -Np0 < ${srcdir}/gcc-ada-repro.patch

    install -dm755 ${pkgbase}-build libgccjit-build
}

build() {
    cd ${pkgbase}-${pkgver}

    local _confflags=(
        --target=${CHOST}
        --mandir=/usr/share/man
        --infodir=/usr/share/info
        --with-build-config=bootstrap-lto
        --with-linker-hash-style=gnu
        --with-system-zlib
        --enable-__cxa_atexit
        --enable-cet=auto
        --enable-checking=release
        --enable-clocale=gnu
        --enable-default-pie
        --enable-default-ssp
        --enable-gnu-indirect-function
        --enable-gnu-unique-object
        --enable-libstdcxx-backtrace
        --enable-link-serialization=1
        --enable-linker-build-id
        --enable-lto
        --disable-multilib
        --enable-plugin
        --enable-shared
        --enable-threads=posix
        --disable-libssp
        --disable-libstdcxx-pch
        --disable-werror
        --with-pkgversion='Futurelinux'
        --with-bugurl=https://gitea.futurelinux.xyz/packages/gcc/issues
    )

    (
         cd ${pkgbase}-build

         CFLAGS=${CFLAGS/-Werror=format-security/}
         CXXFLAGS=${CXXFLAGS/-Werror=format-security/}

         ${BUILD_CONFIGURE}         \
             --enable-languages=ada,c,c++,d,fortran,go,lto,objc,m2,obj-c++,rust \
             --enable-bootstrap \
             "${_confflags[@]:?_confflags unset}"

         # see https://bugs.archlinux.org/task/71777 for rationale re *FLAGS handling
         make -O STAGE1_CFLAGS="-O2"             \
                 BOOT_CFLAGS="${CFLAGS}"         \
                 BOOT_LDFLAGS="${LDFLAGS}"       \
                 LDFLAGS_FOR_TARGET="${LDFLAGS}" \
                 bootstrap

         # make documentation
         make -O -C ${CHOST}/libstdc++-v3/doc doc-man-doxygen

    )

    (
        # Build libgccjit separately, to avoid building all compilers with --enable-host-shared
        # which brings a performance penalty
        cd libgccjit-build

        CFLAGS=${CFLAGS/-Werror=format-security/}
        CXXFLAGS=${CXXFLAGS/-Werror=format-security/}

        ${BUILD_CONFIGURE}         \
            --enable-languages=jit \
            --disable-bootstrap    \
            --enable-host-shared   \
            "${_confflags[@]:?_confflags unset}"

        # see https://bugs.archlinux.org/task/71777 for rationale re *FLAGS handling
        make -O STAGE1_CFLAGS="-O2"             \
                 BOOT_CFLAGS="${CFLAGS}"         \
                 BOOT_LDFLAGS="${LDFLAGS}"       \
                 LDFLAGS_FOR_TARGET="${LDFLAGS}" \
                 all-gcc

        cp -a gcc/libgccjit.so* ../${pkgbase}-build/gcc/
    )
}

package_gcc-libs() {
    pkgdesc='Runtime libraries shipped by GCC'
    groups=('base')
    depends=('glibc')
    options=('!emptydirs' '!strip')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C ${CHOST}/libgcc DESTDIR=${pkgdir} install-shared

    rm -f ${pkgdir}/usr/lib64/gcc/${CHOST}/${pkgver%%+*}/libgcc_eh.a

    for lib in libatomic libgfortran libgo libgomp libitm libquadmath \
        libsanitizer/{a,l,ub,t}san libstdc++-v3/src libvtv
    do
        make -C ${CHOST}/${lib} DESTDIR=${pkgdir} install-toolexeclibLTLIBRARIES
    done

    make -C ${CHOST}/libobjc DESTDIR=${pkgdir} install-libs
    make -C ${CHOST}/libstdc++-v3/po DESTDIR=${pkgdir} install

    make -C ${CHOST}/libphobos DESTDIR=${pkgdir} install
    rm -rf ${pkgdir}/usr/lib64/gcc/${CHOST}/${pkgver%%+*}/include/d
    rm -f ${pkgdir}/usr/lib64/libgphobos.spec

    for lib in libgomp libitm libquadmath; do
        make -C ${CHOST}/${lib} DESTDIR=${pkgdir} install-info
    done
}

package_gcc() {
    pkgdesc="The GNU Compiler Collection - C and C++ frontends"
    groups=('base-devel')
    depends=("${pkgbase}-libs=${pkgver}-${pkgrel}" 'binutils' 'mpc' 'isl' 'zstd')
    options=('!emptydirs' 'staticlibs')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} install-driver install-cpp install-gcc-ar \
        c++.install-common install-headers install-plugin install-lto-wrapper

    install -m755 -t ${pkgdir}/usr/bin/ gcc/gcov{,-tool}
    install -m755 -t ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}/ gcc/{cc1,cc1plus,collect2,lto1}

    make -C ${CHOST}/libgcc DESTDIR=${pkgdir} install
    rm -f ${pkgdir}/usr/lib64/libgcc_s.so*

    make -C ${CHOST}/libstdc++-v3/src DESTDIR=${pkgdir} install
    make -C ${CHOST}/libstdc++-v3/include DESTDIR=${pkgdir} install
    make -C ${CHOST}/libstdc++-v3/libsupc++ DESTDIR=${pkgdir} install
    make -C ${CHOST}/libstdc++-v3/python DESTDIR=${pkgdir} install

    make DESTDIR=${pkgdir} install-libcc1

    install -d ${pkgdir}/usr/share/gdb/auto-load/usr/lib64
    mv ${pkgdir}/usr/lib64/libstdc++.so.6.*-gdb.py ${pkgdir}/usr/share/gdb/auto-load/usr/lib64/

    rm ${pkgdir}/usr/lib64/libstdc++.so*

    make DESTDIR=${pkgdir} install-fixincludes

    make -C gcc DESTDIR=${pkgdir} install-mkheaders

    make -C lto-plugin DESTDIR=${pkgdir} install

    install -dm755 ${pkgdir}/usr/lib64/bfd-plugins/
    ln -s /usr/libexec/gcc/${CHOST}/${pkgver%%+*} ${pkgdir}/usr/lib64/bfd-plugins/

    make -C ${CHOST}/libgomp DESTDIR=${pkgdir} install-nodist_{libsubinclude,toolexeclib}HEADERS
    make -C ${CHOST}/libitm DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS
    make -C ${CHOST}/libquadmath DESTDIR=${pkgdir} install-nodist_libsubincludeHEADERS
    make -C ${CHOST}/libsanitizer DESTDIR=${pkgdir} install-nodist_{saninclude,toolexeclib}HEADERS
    make -C ${CHOST}/libsanitizer/asan DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS
    make -C ${CHOST}/libsanitizer/tsan DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS
    make -C ${CHOST}/libsanitizer/lsan DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS

    make -C gcc DESTDIR=${pkgdir} install-man install-info

    rm ${pkgdir}/usr/share/man/man1/{gccgo,gfortran,lto-dump,gdc,gm2}.1
    rm ${pkgdir}/usr/share/info/{gccgo,gfortran,gnat-style,gnat_rm,gnat_ugn,gdc,m2}.info

    make -C libcpp DESTDIR=${pkgdir} install
    make -C gcc DESTDIR=${pkgdir} install-po

    # many packages expect this symlink
    ln -s gcc ${pkgdir}/usr/bin/cc

    # Create a symlink required by the FHS for "historical" reasons.
    ln -svr cpp ${pkgdir}/usr/lib64

    # POSIX conformance launcher scripts for c89 and c99
    install -Dm755 ${srcdir}/c89 ${pkgdir}/usr/bin/c89
    install -Dm755 ${srcdir}/c99 ${pkgdir}/usr/bin/c99

    # install the libstdc++ man pages
    make -C ${CHOST}/libstdc++-v3/doc DESTDIR=${pkgdir} doc-install-man

    # byte-compile python libraries
    python3 -m compileall ${pkgdir}/usr/share/gcc-${pkgver%%+*}/
    python3 -O -m compileall ${pkgdir}/usr/share/gcc-${pkgver%%+*}/

}

package_gcc-fortran() {
    pkgdesc='Fortran front-end for GCC'
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C ${CHOST}/libgfortran DESTDIR=${pkgdir} install-cafexeclibLTLIBRARIES \
        install-{toolexeclibDATA,nodist_fincludeHEADERS,gfor_cHEADERS}

    make -C ${CHOST}/libgomp DESTDIR=${pkgdir} install-nodist_fincludeHEADERS

    make -C gcc DESTDIR=${pkgdir} fortran.install-{common,man,info}

    install -Dm755 gcc/f951 ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}/f951

    ln -s gfortran ${pkgdir}/usr/bin/f95
}

package_gcc-objc() {
    pkgdesc='Objective-C front-end for GCC'
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C ${CHOST}/libobjc DESTDIR=${pkgdir} install-headers

    install -dm755 ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}
    install -m755 gcc/cc1obj{,plus} ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}/

    mv ${pkgdir}/usr/lib ${pkgdir}/usr/lib64
}

package_gcc-gnat() {
    pkgdesc='Ada front-end for GCC (GNAT)'
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')
    options=('!emptydirs' 'staticlibs')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} ada.install-{common,man,info}

    install -m755 gcc/gnat1 ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}


    make -C ${CHOST}/libada DESTDIR=${pkgdir} INSTALL="install" INSTALL_DATA="install -m644" install-libada

    ln -s gcc ${pkgdir}/usr/bin/gnatgcc

    # insist on dynamic linking, but keep static libraries because gnatmake complains
    mv ${pkgdir}/usr/lib64/gcc/${CHOST}/${pkgver%%+*}/adalib/libgna{rl,t}-${pkgver%%.*}.so ${pkgdir}/usr/lib64

    ln -s libgnarl-${pkgver%%.*}.so ${pkgdir}/usr/lib64/libgnarl.so
    ln -s libgnat-${pkgver%%.*}.so ${pkgdir}/usr/lib64/libgnat.so
    rm -f ${pkgdir}/usr/lib64/gcc/${CHOST}/${pkgver%%+*}/adalib/libgna{rl,t}.so

}

package_gcc-go() {
    pkgdesc='Go front-end for GCC'
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C ${CHOST}/libgo DESTDIR=${pkgdir} install-exec-am

    make DESTDIR=${pkgdir} install-gotools

    make -C gcc DESTDIR=${pkgdir} go.install-{common,man,info}

    rm -f ${pkgdir}/usr/lib64/libgo.so*
    install -Dm755 gcc/go1 ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}/go1

}

package_gcc-gdc() {
    pkgdesc="D frontend for GCC"
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')
    options=('staticlibs')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} d.install-{common,man,info}

    install -Dm755 gcc/gdc ${pkgdir}/usr/bin/gdc
    install -Dm755 gcc/d21 ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}/d21

     make -C ${CHOST}/libphobos DESTDIR=${pkgdir} install
     rm -f ${pkgdir}/usr/lib64/lib{gphobos,gdruntime}.so*

}

package_gcc-m2() {
    pkgdesc='Modula-2 frontend for GCC'
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} m2.install-{common,man,info}

    install -Dm755 gcc/cc1gm2 ${pkgdir}/usr/libexec/gcc/${CHOST}/${pkgver%%+*}/cc1gm2
    install -Dm755 gcc/gm2 ${pkgdir}/usr/bin/gm2

    make -C ${CHOST}/libgm2 DESTDIR=${pkgdir} install

}

package_gcc-rust() {
    pkgdesc="Rust frontend for GCC"
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} rust.install-{common,man,info}

    install -Dm755 gcc/gccrs ${pkgdir}/usr/bin/gccrs
    install -Dm755 gcc/crab1 ${pkgdir}/usr/bin/crab1
}

package_gcc-lto-dump() {
    pkgdesc="Dump link time optimization object files"
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} lto.install-{common,man,info}

}

package_gcc-libgccjit() {
    pkgdesc="Just-In-Time Compilation with GCC backend"
    depends=("${pkgbase}=${pkgver}-${pkgrel}" 'isl')

    cd ${pkgbase}-${pkgver}/${pkgbase}-build

    make -C gcc DESTDIR=${pkgdir} jit.install-common jit.install-info

}
